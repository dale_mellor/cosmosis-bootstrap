# cosmosis-bootstrap

This project contains scripts for the bootstrapping of CosmoSIS. You
can use it to do an initial installation of CosmoSIS; it will download
and install the most recent version of CosmosSIS, and the
corresponding versions of all of its dependencies.

CosmoSIS itself resides in the repository at https://bitbucket.org/joezuntz/cosmosis/wiki/Home.

# Using pre-release versions of the bootstrap

Periodically, we create a pre-release version of the CosmoSIS bootstrap system, and make them available to interested users. Typically these new versions of the bootstrap system include changes in low-level infrastructure, but not the scientific content or capabilities of CosmoSIS. This usually proceeds by creating branches in both the cosmosis-bootstrap repository and cosmosis repository. To try such a pre-release version of the bootstrap, one needs to obtain the pre-release version of the `cosmosis-bootstrap` script, and then to run it. Except for how one obtains the script, this procedure is the same as one follows when installing a released version of CosmoSIS. The steps are as follows. The value of `<branchname>` determines what branch in the CosmoSIS repository is to be used.
```
#!bash
$> cd /path/to/directory/for/installation
$> curl --fail -O https://bitbucket.org/mpaterno/cosmosis-bootstrap/raw/master/cosmosis-bootstrap
$> chmod u+x cosmosis-bootstrap
$> ./cosmosis-bootstrap -t <branchname> cosmosis | tee bootstrap.log 2>&1
$> cd cosmosis
$> source config/setup-cosmosis | tee setup.log 2>&1
$> make | tee make.log 2>&1
```

If any of step fails, please file an issue, and append to that issue any log files created during the attempted installation.
